//
//  AddTeamnameVM.swift
//  Competition Creator
//
//  Created by Apple on 19/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AddTeamnameVM: BaseTableViewVM {
    
    let nibICell = "AddTeamNameTVCell"
    let identifierItemCell = "AddTeamNameTVCell"
    var teamCount:Int!
    var tf_array:[UITextField] = []
    
    init(controller: UIViewController?, itemCount:Int) {
        super.init(controller: controller)
        teamCount = itemCount
    }
    
    //Tableview Methords
    override func getCellForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        print(tableView)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierItemCell)as! AddTeamNameTVCell
        let number = String(indexPath.row+1)
        cell.lblTeamName.text = "Team " + number + " Name"
        
        cell.TxtEnterTeamName.tag = indexPath.row
        if (tf_array.count == 0){
            tf_array.append(cell.TxtEnterTeamName)
        } else {
            if (tf_array.contains(cell.TxtEnterTeamName) == false){
                tf_array.append(cell.TxtEnterTeamName)
            }
        }
        
        return cell
        
    }
    override func getNumbersOfRows(in section: Int) -> Int {
        return teamCount
    }
    override func didSelectRowAt(_ indexPath: IndexPath, tableView: UITableView) {
    }
    
    override func getHeightForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> CGFloat {
        return cellHeight
    }
    
    override func getHeightForHeaderAt(_ section: Int, tableView: UITableView) -> CGFloat {
        return baseHeaderTableViewHeight
    }
    
    override func getBaseTableHeaderViewFor(_ section: Int, tableView: UITableView) -> UIView? {
        //   baseTableHeaderView = BaseTableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: baseHeaderTableViewHeight))
        //  baseTableHeaderView.setInfo(title: sections[section], hint: nil, imageFilterButton: nil)
        return baseTableHeaderView
    }
    
    func addTeamIndb(completionHandler: @escaping (_ result: String) -> Void)
    {
        //print("tffarrr = \(tf_array)")
        for tf in tf_array
        {
           // print("ttttt = \(tf.text)")
            let dictObj : NSMutableDictionary = [:]
            dictObj.setValue("\(tf.text!)", forKey: "team_name")
            if var team_id = userDef.object(forKey: "team_id") as? Int {
                team_id += 1
                dictObj.setValue("\(team_id)", forKey: "team_id")
                userDef.set(team_id, forKey: "team_id")
            }
            else{
                let team_id = 0
                dictObj.setValue("\(team_id)", forKey: "team_id")
                userDef.set(team_id, forKey: "team_id")
            }
            arrDataObject.add(dictObj)
        }
        do {
            for i in 0..<arrDataObject.count{
                let dict = arrDataObject[i] as! NSDictionary
               // let item = TeamNameListRealm(value: dict)
               // DBManager.sharedInstance.addData(object: item)
            }
            completionHandler("Team created Successfully!")
        }
    }
}
