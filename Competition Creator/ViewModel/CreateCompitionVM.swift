//
//  CreateCompitionVM.swift
//  Competition Creator
//
//  Created by Apple on 18/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CreateCompitionVM: BaseTableViewVM {
    
    let nibICell = "CoustomTeamTCVell"
    let identifierItemCell = "CoustomTeamTCVell"
    
   // let realm = try? Realm()
var items = [String]()
    
  //  let items = try! Realm().objects(TeamNameListRealm.self).sorted(byKeyPath: "team_name")
    
    override init(controller: UIViewController?) {
           super.init(controller: controller)
           setValues()
         //  cellHeight = 500.0
       }
    private func setValues(){
       items = ["Custom Team One","Custom Team Two","Custom Team Three","Custom Team Four","Custom Team Five"]
        
    }
    
    //Tableview Methords
    override func getCellForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierItemCell)as! CoustomTeamTCVell
        let dict = items[indexPath.row]
        cell.lblCustomTeamName.text = dict
           return cell
           
       }
       override func getNumbersOfRows(in section: Int) -> Int {
        return items.count
       }
       override func didSelectRowAt(_ indexPath: IndexPath, tableView: UITableView) {
        let cell = tableView.cellForRow(at: indexPath) as! CoustomTeamTCVell
        cell.imgCheck.image = #imageLiteral(resourceName: "tick")
       }

       override func getHeightForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> CGFloat {
           return cellHeight
       }
       
       override func getHeightForHeaderAt(_ section: Int, tableView: UITableView) -> CGFloat {
           return baseHeaderTableViewHeight
       }
       
       override func getBaseTableHeaderViewFor(_ section: Int, tableView: UITableView) -> UIView? {
        //   baseTableHeaderView = BaseTableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: baseHeaderTableViewHeight))
         //  baseTableHeaderView.setInfo(title: sections[section], hint: nil, imageFilterButton: nil)
           return baseTableHeaderView
       }
    @objc private func btnEyeAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
       
}
