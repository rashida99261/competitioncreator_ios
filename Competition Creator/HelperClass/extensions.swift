//
//  extensions.swift
//  Competition Creator
//
//  Created by Apple on 08/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController
{
    func presentViewControllerBasedOnIdentifier(_ strIdentifier:String)
    {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let destViewController : UIViewController = story.instantiateViewController(withIdentifier: strIdentifier)
        self.navigationController?.pushViewController(destViewController, animated: true)
        
    }
}

