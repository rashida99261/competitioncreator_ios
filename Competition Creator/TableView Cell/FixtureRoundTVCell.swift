//
//  FixtureRoundTVCell.swift
//  Competition Creator
//
//  Created by Apoorva on 23/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class FixtureRoundTVCell: UITableViewCell {
 
    @IBOutlet weak var lblTeamPlayer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
