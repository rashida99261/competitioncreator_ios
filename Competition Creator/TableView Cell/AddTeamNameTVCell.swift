//
//  AddTeamNameTVCell.swift
//  Competition Creator
//
//  Created by Apple on 19/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
var arrDataObject = NSMutableArray()

class AddTeamNameTVCell: UITableViewCell , UITextFieldDelegate{
    
    @IBOutlet weak var lblTeamName : UILabel!
    @IBOutlet weak var TxtEnterTeamName : UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        TxtEnterTeamName.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        let dictObj : NSMutableDictionary = [:]
//        dictObj.setValue("\(textField.text!)", forKey: "team_name")
//        if var team_id = userDef.object(forKey: "team_id") as? Int {
//            team_id += 1
//            dictObj.setValue("\(team_id)", forKey: "team_id")
//            userDef.set(team_id, forKey: "team_id")
//        }
//        else{
//            let team_id = 0
//            dictObj.setValue("\(team_id)", forKey: "team_id")
//            userDef.set(team_id, forKey: "team_id")
//        }
//        arrDataObject.add(dictObj)
        return true
    }
}
