//
//  fixturesViewController.swift
//  Competition Creator
//
//  Created by Apple on 10/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class fixturesViewController: UIViewController {
    @IBOutlet weak var tblRound: UITableView!
    
    @IBOutlet weak var lblCompetitionName: UILabel!
    
    var count = 1
    var isBreakLoop = false
    var viewModel : fixturesVM!
    
    var teams: Int = 0
    var ghost = false
    
    var arrList = NSMutableArray()
    
    
    
    //   var arrDemo : NSMutableArray = []
    var arrDemo = [[String]]()
    var  dictt = [String:[NSMutableDictionary]]()
    var roundArray =  [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.generateList(teammsize: "5")
        //recurse()
        
        
    }
    func recurse() {
        //statements
        var arrTemp = [String]()
        
        print(arrDemo.count)
        for var i in count...TeamRound{
            if count != i {
                
                let rt1 = "Team \(count)  Vs  Team \(i)"
                let strRound = "Round \(count)"
                if roundArray.contains(strRound){
                    
                }else{
                    roundArray.append(strRound)
                }
                
                arrTemp.append(rt1)
                
                if i == TeamRound {
                    i = 0
                    count += 1
                }
                if count == TeamRound{
                    isBreakLoop = true
                    break
                }
            }
        }
        
        if isBreakLoop{
            arrDemo.append(arrTemp)
            viewModel = fixturesVM.init(controller: self, items: arrDemo)
        }else{
            arrDemo.append(arrTemp)
            recurse()
        }
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: createCompetitionViewControler.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
}

//BottomAction
extension fixturesViewController
{
    @IBAction func clcikOnBottomTabs(_ sender : UIButton)
    {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let destViewController : UIViewController = story.instantiateViewController(withIdentifier: "ladderViewController") as! ladderViewController
        self.navigationController?.pushViewController(destViewController, animated: false)
    }
}

//tableview Delegae DataSource
extension fixturesViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.arrMain.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        viewModel.arrMain[section].count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        viewModel.getCellForRowAt(indexPath, tableView: tblRound)
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < roundArray.count {
            return roundArray[section]
        }
        
        return nil
    }
    
}




extension fixturesViewController
{
    
    func  generateList(teammsize: String) {
        // Find out how many teams we want fixtures for.
        var teams = 0;
        teams = Int(teammsize)!
        
        // If odd number of teams add a "ghost".
        var ghost = false
        if (teams % 2 == 1) {
            teams = teams+1
            ghost = true
        }
        
        // Generate the fixtures using the cyclic algorithm.
        let totalRounds = teams - 1;
        let matchesPerRound = teams / 2;
        
        
        for round in 0..<totalRounds
        {
            let arr = NSMutableArray()
            for match in 0..<matchesPerRound {
                
                let home = (round + match) % (teams - 1);
                var away = (teams - 1 - match + round) % (teams - 1);
                // Last team stays in the same place while the others
                // rotate around it.
                if (match == 0) {
                    away = teams - 1;
                }
                // Add one so teams are number 1 to teams not 0 to teams - 1
                // upon display.
                let obj : NSMutableDictionary = [:]
                obj.setValue((home + 1), forKey: "first_team")
                obj.setValue((away + 1), forKey: "second_team")
                arr.add(obj)
            }
            arrList.add(arr)
        }
        // Interleave so that home and away games are fairly evenly dispersed.
        var tempList = NSMutableArray()
        
        
        for i in 0..<5{
            
            let dict = NSMutableArray()
            tempList.insert(dict, at: i)
        }
        // Interleave so that home and away games are fairly evenly dispersed.
        
        var evn1 = 0;
        var odd1 = (teams / 2);
        
        for i in 0..<arrList.count{
            if (i % 2 == 0) {
                let dict = arrList[evn1]
                tempList.replaceObject(at: i, with: dict)
                evn1 = evn1+1
            } else {
                let dict = arrList[odd1]
                tempList.replaceObject(at: i, with: dict)
                odd1 = odd1+1
            }
        }
        arrList = []
        arrList = tempList
        
        print("jsonArrayList: \(arrList)")
        
        //            // Last team can't be away for every game so flip them
        //            // to home on odd rounds.
        for k in 0..<arrList.count{
            if (k % 2 == 1) {
                let jsonArray = arrList[k] as! NSMutableArray
                print(jsonArray)
                let www = flipNew(match: jsonArray)
                arrList.replaceObject(at: k, with: www)
            } else {
                print(k)
            }
            
        }
        
        print("jsonArrayList: \(arrList)")
        
        for i in 0..<arrList.count{
            print("TAG Round: \((i + 1))");
            let aaa = arrList[i] as! NSMutableArray
            for j in 0..<aaa.count {
                let object = aaa[j] as! NSDictionary
                let fteam = object["first_team"]
                let lteam = object["second_team"]
                print("\(fteam)  V  \(lteam)")
            }
        }
        
        if (ghost) {
            print("TAG-----\(teams) are byes");
        }
    }
    
    func flipNew (match: NSMutableArray) -> NSMutableArray {
        let components = match[0] as! NSDictionary
        let dict : NSMutableDictionary = [:]
        let ftema = components["first_team"]
        let stema = components["second_team"]
        dict.setValue(stema, forKey: "first_team")
        dict.setValue(ftema, forKey: "second_team")
        match.replaceObject(at: 0, with: dict)
        return match
    }
}







