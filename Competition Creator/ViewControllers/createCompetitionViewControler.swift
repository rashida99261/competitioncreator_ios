//
//  createCompetitionViewControler.swift
//  Competition Creator
//
//  Created by Apple on 08/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
var TeamRound = 0
class createCompetitionViewControler: UIViewController {
    
    @IBOutlet weak var txtTeamTame : UITextField!
    
    @IBOutlet weak var lblTeamNo : UILabel!
    @IBOutlet weak var lblPlayerNo : UILabel!
    
    @IBOutlet weak var btnDefaultOutlet: UIButton!
    
    @IBOutlet weak var btnCustomTeamOutlet: UIButton!
    
    @IBOutlet weak var viewTransparent: UIView!
    
    @IBOutlet weak var tblCustomTeam: UITableView!
    @IBOutlet weak var lblCompitionNameAlart: UILabel!
    
    @IBOutlet weak var btnPont2 : UIButton!
    @IBOutlet weak var btnPont4 : UIButton!
    @IBOutlet weak var btnPont6 : UIButton!
    
    @IBOutlet weak var lblResult : UILabel!
    
    var teamNo = 0
    var playerNo = 1
    var pointsWin = 2
    var viewModel:CreateCompitionVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CreateCompitionVM.init(controller: self)
        
        if(viewModel.items.count > 0){
            self.lblResult.isHidden = true
            self.tblCustomTeam.isHidden = false
            self.tblCustomTeam.reloadData()
        }
        else{
            self.lblResult.isHidden = false
            self.tblCustomTeam.isHidden = true
        }
        
        self.viewTransparent.alpha = 0
        self.tblCustomTeam.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickONCreatCompetion(_ sender : UIButton)
    {
        let teamNum = Int(lblTeamNo.text!)
        TeamRound = teamNum!*(teamNum!-1) / 2
        self.presentViewControllerBasedOnIdentifier("fixturesViewController")
    }
    
    @IBAction func clcikOnBtnChooseTeamNo(_ sender : UIButton)
    {
        lblCompitionNameAlart.text = txtTeamTame.text
        if(sender.tag == 10){
            teamNo += 1
            self.lblTeamNo.text = "\(teamNo)"
            if teamNo > 0{
                //viewAddTeam.isHidden = false
            }
        }
        else if(sender.tag == 20){
            if(teamNo > 0){
                teamNo -= 1
                self.lblTeamNo.text = "\(teamNo)"
            }
            if teamNo == 0 {
            }
        }
    }
    
    @IBAction func clcikOnBtnChoosePlayerNo(_ sender : UIButton)
    {
        if(sender.tag == 10){
            if playerNo <= 2{
                if playerNo != 2{
                    playerNo += 1
                }
                self.lblPlayerNo.text = "\(playerNo)"
            }
        }
        else if(sender.tag == 20){
            if(playerNo > 1){
                playerNo -= 1
                self.lblPlayerNo.text = "\(playerNo)"
            }
        }
    }
    
    @IBAction func clcikOnBtnChoosePintsWin(_ sender : UIButton)
    {
        if(sender.tag == 10){
            pointsWin = 2
            btnPont2.backgroundColor = .lightGray
            btnPont4.backgroundColor = .clear
            btnPont6.backgroundColor = .clear
        }
        else if(sender.tag == 20){
            pointsWin = 4
            btnPont4.backgroundColor = .lightGray
            btnPont2.backgroundColor = .clear
            btnPont6.backgroundColor = .clear
        }
        else if(sender.tag == 30){
            pointsWin = 6
            btnPont6.backgroundColor = .lightGray
            btnPont2.backgroundColor = .clear
            btnPont4.backgroundColor = .clear
            
        }
    }
    @IBAction func clickoNChooseTeamName(_ sender : UIButton){
        if sender.tag == 15{
            btnDefaultOutlet.isSelected = true
            btnCustomTeamOutlet.isSelected = false
            
        }else if sender.tag == 30{
            btnDefaultOutlet.isSelected = false
            btnCustomTeamOutlet.isSelected = true
            
            if(self.lblTeamNo.text == "0"){
                self.btnDefaultOutlet.isSelected = false
                self.btnCustomTeamOutlet.isSelected = false
                Globalfunc.showAlertMessage(vc: self, titleStr: "", messageStr: "Team should be Selected.Please select how many Teams to create.")
            }
            else{
                UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
                    self.viewTransparent.alpha = 1
                }) { _ in
                }
            }
        }
    }
    
    
    
    @IBAction func btnCrossAction(_ sender: Any) {
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
            self.viewTransparent.alpha = 0
        }) { _ in
            
            self.btnDefaultOutlet.isSelected = false
            self.btnCustomTeamOutlet.isSelected = false

        }
    }
    
    @IBAction func btnAddTeamAction(_ sender: Any) {
        let destViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTeamnameVC") as! AddTeamnameVC
        destViewController.teamCount = teamNo
        destViewController.strCompitionname =  txtTeamTame.text!
        self.navigationController?.pushViewController(destViewController, animated: true)
        
    }
}
extension createCompetitionViewControler:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumbersOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCellForRowAt(indexPath, tableView: tblCustomTeam)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return viewModel.didSelectRowAt(indexPath, tableView: tblCustomTeam)
    }
}
