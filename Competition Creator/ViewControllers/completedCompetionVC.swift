//
//  completedCompetionVC.swift
//  Competition Creator
//
//  Created by Apple on 11/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class completedCompetionVC: UIViewController {

    
    @IBOutlet weak var tblCompNameList : UITableView!
    var arrTeam = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblCompNameList.tableFooterView = UIView()
        self.setTeamName()
        
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
         self.navigationController?.popViewController(animated: true)
    }
    
    func setTeamName(){
        for i in 1..<15
        {
            arrTeam.append("Competion Name \(i)")
            self.tblCompNameList.reloadData()
        }
    }
    
   
}

extension completedCompetionVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTeam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblCompNameList.dequeueReusableCell(withIdentifier: "tblCompNameCell") as! tblCompNameCell
        cell.lblTeamName.text = self.arrTeam[indexPath.row]
        return cell
    }
}

class tblCompNameCell : UITableViewCell
{
    @IBOutlet weak var lblTeamName : UILabel!
}
