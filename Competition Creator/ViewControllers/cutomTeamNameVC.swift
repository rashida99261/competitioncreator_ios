//
//  cutomTeamNameVC.swift
//  Competition Creator
//
//  Created by Apple on 09/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class cutomTeamNameVC: UIViewController {
    
    @IBOutlet weak var tblTeamNameList : UITableView!
    var arrTeam = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblTeamNameList.tableFooterView = UIView()
        self.setTeamName()
        
        
       //  print(Realm.Configuration.defaultConfiguration.fileURL!)
        
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
         self.navigationController?.popViewController(animated: true)
    }
    
    func setTeamName(){
        
        for i in 1..<15
        {
            arrTeam.append("Team \(i)")
            self.tblTeamNameList.reloadData()
        }
    }
    
   
}

extension cutomTeamNameVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTeam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblTeamNameList.dequeueReusableCell(withIdentifier: "tblTeamNameCell") as! tblTeamNameCell
        cell.lblTeamName.text = self.arrTeam[indexPath.row]
        return cell
    }
}

class tblTeamNameCell : UITableViewCell
{
    @IBOutlet weak var lblTeamName : UILabel!
}
