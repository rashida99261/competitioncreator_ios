//
//  currentcompetitionVC.swift
//  Competition Creator
//
//  Created by Apple on 09/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit



class ladderViewController: UIViewController {
    
    var arrColumn : [NSMutableArray] = []
    
    @IBOutlet weak var gridCollectionView: UICollectionView! {
           didSet {
               gridCollectionView.bounces = false
           }
       }
       
       @IBOutlet weak var gridLayout: StickyGridCollectionViewLayout! {
           didSet {
               gridLayout.stickyRowsCount = 1
               gridLayout.stickyColumnsCount = 1
           }
       }
                    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setColumnData()
        self.gridCollectionView.reloadData()

        // Do any additional setup after loading the view.
    }
    
    func setColumnData(){
        
        let dict1 = ["column":"Team"]
        let dict2 = ["column":"Pts"]
        let dict3 = ["column":"For"]
        let dict4 = ["column":"Again"]
        let dict5 = ["column":"Difference"]
        
        let arrColm = NSMutableArray()
        arrColm.add(dict1)
        arrColm.add(dict2)
        arrColm.add(dict3)
        arrColm.add(dict4)
        arrColm.add(dict5)
        
        self.arrColumn.insert(arrColm, at: 0)
        print(self.arrColumn)
        
        
    }
    

   @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: createCompetitionViewControler.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

}
// MARK: - Collection view data source and delegate methods

extension ladderViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrColumn.count//row
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5 //column
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ladderCell", for: indexPath) as? ladderCell else {
            return UICollectionViewCell()
        }
        
        if let arrInside = self.arrColumn[indexPath.section][indexPath.row] as? NSDictionary
        {
            let columnNam = arrInside["column"] != nil
            if(columnNam){
                    cell.lbltitle.text = "\(arrInside["column"] as! String)"
                    cell.lbltitle.textColor = .white
                    cell.backgroundColor = .black
                   cell.lblLine.backgroundColor = .white
            }
            else{
                cell.lbltitle.text = "\(arrInside["value"] as! String)"
                cell.lbltitle.textColor = .black
                cell.backgroundColor = .white
                cell.lblLine.backgroundColor = .black
            }
        }
        return cell
    }
}

extension ladderViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2, height: 50)
    }
}

class ladderCell: UICollectionViewCell {
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblLine: UILabel!
}


//BottomAction
extension ladderViewController
{
    @IBAction func clcikOnBottomTabs(_ sender : UIButton)
    {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let destViewController : UIViewController = story.instantiateViewController(withIdentifier: "fixturesViewController") as! fixturesViewController
        self.navigationController?.pushViewController(destViewController, animated: false)
       }
}
