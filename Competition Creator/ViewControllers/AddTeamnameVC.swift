//
//  AddTeamnameVC.swift
//  Competition Creator
//
//  Created by Apple on 19/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AddTeamnameVC: UIViewController {

  
    @IBOutlet weak var lblCopetitionName: UILabel!
    
    @IBOutlet weak var tblCreateTeamName: UITableView!
    
    var viewModel:AddTeamnameVM!
    var teamCount = 0
    var strCompitionname = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCopetitionName.text = strCompitionname
        viewModel = AddTeamnameVM.init(controller: self, itemCount: teamCount)
        // Do any additional setup after loading the view.
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
      }
    
    @IBAction func btnclickOnAddTeam(_ sender: UIButton)
    {
        viewModel.addTeamIndb { (result) in
            
            let alertController = UIAlertController(title: "Alert", message: result, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                self.navigationController?.popToRootViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
}
//Tableview Delegate Data Source
extension AddTeamnameVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumbersOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCellForRowAt(indexPath, tableView: tblCreateTeamName)
    }
    
    
}
