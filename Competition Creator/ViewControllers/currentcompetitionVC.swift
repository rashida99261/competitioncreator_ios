//
//  currentcompetitionVC.swift
//  Competition Creator
//
//  Created by Apple on 10/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class currentcompetitionVC: UIViewController {
    
    @IBOutlet weak var tblCurrntCompList : UITableView!
    
    var aarListData : NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblCurrntCompList.tableFooterView = UIView()
        self.setColumnData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setColumnData(){
           
           
           let dict1 = ["title":"Boys Success","value":"13 of 42"]
           let dict2 = ["title":"Girls Success","value":"0 of 10"]
           
           self.aarListData.add(dict1)
           self.aarListData.add(dict2)
        
            self.tblCurrntCompList.reloadData()
           
           
       }
}

extension currentcompetitionVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aarListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblCurrntCompList.dequeueReusableCell(withIdentifier: "currCompetionTblCell") as! currCompetionTblCell
        
        let dict = self.aarListData[indexPath.row] as! NSDictionary
        cell.lblCompName.text = (dict["title"] as! String)
        cell.lblResult.text = (dict["value"] as! String)
        return cell
    }
}

class currCompetionTblCell : UITableViewCell
{
    @IBOutlet weak var lblCompName : UILabel!
    @IBOutlet weak var lblResult : UILabel!
}
